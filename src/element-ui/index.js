import {ElButton, ElSelect} from "element-plus";
import 'element-plus/lib/theme-chalk/index.css'

const install = (Vue) => {
    Vue.component(ElButton.name, ElButton);
    Vue.component(ElSelect.name, ElSelect);
}

export default install