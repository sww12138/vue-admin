import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Layout from '../views/layouts/index.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Layout',
        component: Layout,
        redirect: '/index',
        children: [
            {
                path: 'index',
                name: 'Index',
                component: () => import( '../views/index/index.vue')
            }
        ]
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
