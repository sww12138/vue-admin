// @ts-ignore
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// @ts-ignore
import elementUi from './element-ui'

const app = createApp(App).use(store).use(router)
elementUi(app)
app.mount('#app')
