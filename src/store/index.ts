import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";

export default createStore({
    state: {
        vuex_count: 0,
    },
    mutations: {
        setCount(state, num: number): void {
            state.vuex_count += num
        }
    },
    actions: {
    },
    modules: {
    },
    plugins: [
        createPersistedState({
            storage: {
                getItem: (key) => localStorage.getItem(key),
                setItem: (key, value) => localStorage.setItem(key, value),
                removeItem: (key) => localStorage.removeItem(key),
            }
        })
    ]
})
