import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    port: 8096,//端口
    open: true,//是否自动打开浏览器
    proxy: undefined, //代理
  },
})
